package com.repin.updateClient.controller.progress;

import com.repin.updateClient.model.Status;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;

import java.net.URL;
import java.util.ResourceBundle;

public class ProgressController implements Initializable {

    @FXML
    public TextArea txtProgress;
    @FXML
    public ProgressBar progressBar;
    @FXML
    public Label lblStatus;

    public static Status status = new Status();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                txtProgress.textProperty().bind(status.progressTextProperty());
                progressBar.progressProperty().bind(status.progressDownloadProperty());
                lblStatus.textProperty().bind(status.statusLabelProperty());
            }
        });


    }
}
