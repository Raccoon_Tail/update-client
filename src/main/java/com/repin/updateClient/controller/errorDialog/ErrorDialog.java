package com.repin.updateClient.controller.errorDialog;

import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Optional;

/**
 * Обработчик ошибок
 */
public class ErrorDialog {

    /**
     * Формирует окно с ошибкой и стек-трейсом
     *
     * @param ex Стек-трейс
     * @param msgUser Сообщение для пользователя
     * @param msgProgrammer Сообщение для програмиста
     */
    public static void showError(Exception ex, String msgUser, String msgProgrammer) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Ошибка!");
        alert.setHeaderText(msgUser);
        alert.setContentText(msgProgrammer);
        // Трек ошибки
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("The exception stacktrace was:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

    /**
     * Формирование окна с предупреждением
     *
     * @param msg Ошибка
     * @param whatneeddo Описание ошибки
     */
    public static void showWarn(String msg, String whatneeddo) {
        Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle("Внимание!");
        alert.setHeaderText(msg);
        alert.setContentText(whatneeddo);
        alert.showAndWait();
        //alert.show();
    }

    /**
     * Формирование окна подтверждения действия пользователя
     *
     * @param msg Утвердительное сообщение (н.п. "Файл будет удален")
     * @param qst Вопросительное сообщение (н.п. "Вы действительно хотите удалить файл?")
     * @param isYesFocused Будет ли выделена по умолчанию кнопка "Да"
     * @return Выбранная кнопка
     */
    public static Optional<ButtonType> showDialog(String msg, String qst, boolean isYesFocused){
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Подтверждение");
        alert.setHeaderText(msg);
        alert.setContentText(qst);
        alert.getButtonTypes().clear();
        alert.getButtonTypes().addAll(ButtonType.YES, ButtonType.NO);
        Button yesButton = (Button) alert.getDialogPane().lookupButton( ButtonType.YES );
        yesButton.setText("Да");
        Button noButton = (Button) alert.getDialogPane().lookupButton( ButtonType.NO );
        noButton.setText("Нет");
        alert.getDialogPane().lookupButton(ButtonType.YES).setOnKeyReleased(event -> {
            if (event.getCode() == KeyCode.ENTER){
                yesButton.fire();
            }
        });
        alert.getDialogPane().lookupButton(ButtonType.NO).setOnKeyReleased(event -> {
            if (event.getCode() == KeyCode.ENTER){
                noButton.fire();
            }
        });
        yesButton.setDefaultButton(false);
        if (isYesFocused){
            yesButton.requestFocus();
        } else {
            noButton.requestFocus();
        }
        //noButton.requestFocus();
        return alert.showAndWait();

    }
}
