package com.repin.updateClient.service;

import com.repin.updateClient.Main;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class SettingsReader {

    public boolean readSettings() {
        try {
            String jarPath = System.getProperty("user.dir");
            FileInputStream fStream = new FileInputStream(jarPath + "\\update_config.ini");
            Scanner scanner = new Scanner(fStream);
            String strLine;
            while (scanner.hasNextLine()) {
                strLine = scanner.nextLine();
                String[] tempLine = strLine.split("=");
                //System.out.println(tempLine[0]);
                switch (tempLine[0]) {
                    case "Path":
                        System.out.println(tempLine[1]);
                        //return Integer.parseInt(tempLine[1]);
                        Main.settings.setPathToSave(tempLine[1]);
                        break;
                    case "ServerIP":
                        Main.settings.setServerIP(tempLine[1]);
                        System.out.println(tempLine[1]);
                        break;
                    case "ServerPort":
                        Main.settings.setServerPort(Integer.parseInt(tempLine[1]));
                        System.out.println(tempLine[1]);
                        break;
                    case "UpdateType":
                        Main.settings.setUpdateType(tempLine[1]);
                        System.out.println(tempLine[1]);
                        break;
                    case "ConnectionTimeout":
                        try {
                            Main.settings.setConnectionTimeout(Integer.parseInt(tempLine[1]));
                        } catch (Exception ignore) {
                        }
                        System.out.println(tempLine[1]);
                        break;
                    case "Login":
                        Main.settings.setLoginString(tempLine[1]);
                        break;
                    case "Password":
                        Main.settings.setPasswordString(tempLine[1]);
                        break;
                    case "Exec":
                        Main.settings.setExecProgram(tempLine[1]);
                        break;
                }
            }
            fStream.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}