package com.repin.updateClient.service;

import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class ZipService {
    String filePath;
    ZipFile zipFileLib;

    public ZipService(String filePath) {
        this.filePath = filePath;
        zipFileLib = new ZipFile(this.filePath);
        zipFileLib.setCharset(Charset.forName("CP866"));
    }

    public boolean unzip(){
        StatusUpdate.updateStatusText("Извлечение...");
        StatusUpdate.updateStatusLabel("Извлечение");
        try {
            zipFileLib.extractAll(PathBuilder.getPathToExtract());
            StatusUpdate.updateStatusText("Извлечение прошло успешно");
            return true;
        } catch (ZipException e){
            e.printStackTrace();
            StatusUpdate.updateStatusText("Ошибка извлечения файлов из архива");
        }
        return false;
    }
}
