package com.repin.updateClient.service;

import org.apache.commons.net.ftp.FTPFile;

import java.io.File;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class FileService {
    private File fileInstance;

    public FileService(String path){
        fileInstance = new File(path);
    }

    public boolean IsFileExist(){
        return fileInstance.exists();
    }

    public boolean isTargetNewer(String lastModifyTarget){
        return Instant.ofEpochMilli(fileInstance.lastModified()).atZone(ZoneId.systemDefault()).toLocalDateTime().isBefore(
                LocalDateTime.parse(lastModifyTarget, DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
    }

    public boolean isDateEqual(String lastModifyTarget){
        return Instant.ofEpochMilli(fileInstance.lastModified()).atZone(ZoneId.systemDefault()).toLocalDateTime().isEqual(
                LocalDateTime.parse(lastModifyTarget, DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
    }

    public boolean isTargetBugger(long size){
        return size > fileInstance.length();
    }

    public File getFileInstance() {
        return fileInstance;
    }
}
