package com.repin.updateClient.service;

import com.repin.updateClient.Main;

public class PathBuilder {
    public static String getCurrentUpdatePath(){
        return Main.settings.getHomePath() +
                "/" +
                Main.settings.getPathToSave() +
                "/updates/" +
                Main.settings.getUpdateType() +
                ".zip";
    }

    public static String getTargetUpdatePath(){
        return Main.settings.getUpdateType() +
                "/" +
                Main.settings.getUpdateType() +
                ".zip";
    }

    public static String getPathToExtract(){
        return Main.settings.getHomePath() +
                Main.settings.getPathToSave() +
                Main.settings.getUpdateType() + "/";
    }

    public static String getExecFilePath(String execName){
        return getPathToExtract() + execName;
    }
}
