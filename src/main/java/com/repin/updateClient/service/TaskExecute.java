package com.repin.updateClient.service;

import com.repin.updateClient.Main;
import javafx.application.Platform;
import javafx.concurrent.Task;

public class TaskExecute {

    public void startFTPTask(){
        Task<Void> ftpTask = new FtpTask();
        Thread thread = new Thread(ftpTask);
        thread.setDaemon(true);
        thread.start();
    }

    private static class FtpTask extends Task<Void>{

        @Override
        protected Void call() throws Exception {
            FtpService ftpUnit = new FtpService();
            ftpUnit.connectFTP();
            ftpUnit.authorizationFTP();
            ftpUnit.checkUpdateFTP();
            if (new LaunchService().launchExecFile(Main.settings.getExecProgram())){
                Platform.setImplicitExit(false);
                Platform.exit();
            }
            return null;
        }
    }
}

