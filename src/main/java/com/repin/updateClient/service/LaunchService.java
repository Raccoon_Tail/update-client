package com.repin.updateClient.service;

import java.io.IOException;

public class LaunchService {
    public boolean launchExecFile(String execProgram){
        StatusUpdate.updateStatusText("Запуск программы");
        StatusUpdate.updateStatusLabel("Запуск программы");
        try {
            new ProcessBuilder(PathBuilder.getExecFilePath(execProgram)).start();
            return true;
        } catch (IOException e){
            StatusUpdate.updateStatusText("Неудачно");
            StatusUpdate.updateStatusText(e.getMessage());
        }
        return false;
    }
}
