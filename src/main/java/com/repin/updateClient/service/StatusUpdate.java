package com.repin.updateClient.service;

import com.repin.updateClient.controller.progress.ProgressController;
import javafx.application.Platform;

public class StatusUpdate {

    public static void updateStatusText(String newText){
        new Thread(() -> Platform.runLater(new Runnable() {
            public void run() {
                ProgressController.status.setProgressText(
                        ProgressController.status.getProgressText() +
                                "\n" +
                                newText
                );
            }
        })).start();

    }

    public static void updateProgressDownload(double newProgress){
        new Thread(() -> Platform.runLater(new Runnable() {
            public void run() {
                ProgressController.status.setProgressDownload(newProgress);
            }
        })).start();

    }

    public static void updateStatusLabel(String newLabelText){
        new Thread(() -> Platform.runLater(new Runnable() {
            public void run() {
                ProgressController.status.setStatusLabel(newLabelText);
            }
        })).start();

    }
}
