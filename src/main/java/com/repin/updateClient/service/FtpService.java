package com.repin.updateClient.service;

import com.repin.updateClient.Main;
import com.repin.updateClient.controller.errorDialog.ErrorDialog;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.io.CopyStreamAdapter;

import java.io.*;

public class FtpService {

    FTPClient ftpClient;
    String currentUpdatePath;
    String targetUpdatePath;

    public FtpService(){
        ftpClient = new FTPClient();
    }

    public void connectFTP(){
        StatusUpdate.updateStatusText("Соединение с: " +
                Main.settings.getServerIP() +
                ":" +
                Main.settings.getServerPort()
        );
        StatusUpdate.updateStatusLabel("Соединение...");
        try {
            ftpClient.setConnectTimeout(Main.settings.getConnectionTimeout());
            ftpClient.connect(Main.settings.getServerIP(), Main.settings.getServerPort());
            StatusUpdate.updateStatusText("Соединение прошло успешно");
        } catch (IOException e){
            StatusUpdate.updateStatusText(e.toString());
            e.printStackTrace();
        }
    }

    public void authorizationFTP(){
        try {
            StatusUpdate.updateStatusLabel("Авторизация...");
            if (ftpClient.login(Main.settings.getLoginString(), Main.settings.getPasswordString())) {
                ftpClient.enterLocalPassiveMode();
                ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
                StatusUpdate.updateStatusText("Авторизация пройдена успешно");
            } else {
                ErrorDialog.showWarn(
                        "Авторизация не пройдена",
                        "Проверьте корректность логина/пароля"
                );
            }
        } catch (IOException e){
            StatusUpdate.updateStatusText(e.toString());
            e.printStackTrace();
        }
    }

    public void checkUpdateFTP(){
        StatusUpdate.updateStatusText("Проверка обновления...");
        StatusUpdate.updateStatusLabel("Проверка обновления");
        targetUpdatePath = PathBuilder.getTargetUpdatePath();
        currentUpdatePath = PathBuilder.getCurrentUpdatePath();
        FileService currentFile = new FileService(currentUpdatePath);
        try {
            String lastModifyFTPFile = ftpClient.getModificationTime(targetUpdatePath);
            if (lastModifyFTPFile != null){
                long size = 0;
                FTPFile[] ff = ftpClient.listFiles(targetUpdatePath);
                if(ff != null)
                {
                    size = ff[0].getSize();
                }
                if (currentFile.IsFileExist()) {
                    if (currentFile.isTargetNewer(lastModifyFTPFile)) {
                        StatusUpdate.updateStatusText("На сервере найдена новая версия");
                        downloadUpdate(currentFile.getFileInstance(), false, size);

                        ZipService zipService = new ZipService(currentUpdatePath);
                        zipService.unzip();
                    } else {
                        if (currentFile.isTargetBugger(size)){
                            StatusUpdate.updateStatusText("Найдена незавершенная загрузка обновления");
                            downloadUpdate(currentFile.getFileInstance(), true, size);

                            ZipService zipService = new ZipService(currentUpdatePath);
                            zipService.unzip();
                        } else {
                            StatusUpdate.updateStatusText("Обновление не требуется");
                        }
                    }
                } else {
                    StatusUpdate.updateStatusText(
                            "Локальный фаил обновления отсутвует\n" +
                            "Проверьте конфигурационные файлы обновляемой программы!"
                    );
                    File f = currentFile.getFileInstance();
                    downloadUpdate(f, false, size);

                    ZipService zipService = new ZipService(currentUpdatePath);
                    zipService.unzip();
                }
            } else {
                StatusUpdate.updateStatusText("Файл обновления не найден на сервере");
            }
        } catch (IOException e){
            StatusUpdate.updateStatusText(e.toString());
            e.printStackTrace();
        }
    }

    private void downloadUpdate(File file, boolean isContinue, long length){
        if (isContinue) {
            StatusUpdate.updateStatusText("Происходит возобновление скачивания обновления");
        } else {
            StatusUpdate.updateStatusText("Происходит скачивание обновления");
        }
        StatusUpdate.updateStatusLabel("Скачивание");
        try {
            final long fileSize = file.length();
            OutputStream outputStream;
            CopyStreamAdapter streamListener = new CopyStreamAdapter() {
                @Override
                public void bytesTransferred(long totalBytesTransferred, int bytesTransferred, long streamSize) {
                    StatusUpdate.updateProgressDownload(
                             ((isContinue ?
                                     (double) (totalBytesTransferred + fileSize) : (double) totalBytesTransferred)
                                     / length)
                    );
                }
            };
            ftpClient.setCopyStreamListener(streamListener);
            if (isContinue){
                outputStream = new BufferedOutputStream(
                        new FileOutputStream(file, true));
                ftpClient.setRestartOffset(file.length());
            } else {
                outputStream = new BufferedOutputStream(
                        new FileOutputStream(file));
            }
            if (ftpClient.retrieveFile(targetUpdatePath, outputStream)){
                StatusUpdate.updateStatusText("Файл обновления загружен");
                StatusUpdate.updateStatusLabel("Загрузка завершена");
            }
            outputStream.close();
        } catch (IOException e){
            StatusUpdate.updateStatusText(e.toString());
            e.printStackTrace();
        }
    }

    public FTPClient getFtpClient() {
        return ftpClient;
    }
}
