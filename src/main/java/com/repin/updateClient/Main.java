package com.repin.updateClient;

import com.repin.updateClient.controller.errorDialog.ErrorDialog;
import com.repin.updateClient.model.Settings;
import com.repin.updateClient.service.SettingsReader;
import com.repin.updateClient.service.TaskExecute;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    public static Settings settings = new Settings();
    private final SettingsReader settingsReader = new SettingsReader();

    @Override
    public void start(Stage primaryStage) throws Exception{
        if (settingsReader.readSettings() && settings.isCorrect()) {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/progress/ProgressView.fxml"));
            Parent root = loader.load();
            primaryStage.setTitle("Менеджер обновлений");
            primaryStage.setScene(new Scene(root));
            primaryStage.show();
            new TaskExecute().startFTPTask();
        } else {
            ErrorDialog.showWarn(
                    "Конфигурационный фаил поврежден",
                    "Программа не будет запущена"
            );
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
