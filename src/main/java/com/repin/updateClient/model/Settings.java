package com.repin.updateClient.model;

import java.io.Serializable;

public class Settings implements Serializable {
    private String serverIP;
    private Integer serverPort;
    private String pathToSave;
    private String updateType;
    private Integer connectionTimeout;
    private String homePath;
    private String loginString;
    private String passwordString;
    private String execProgram;

    public Settings() {
        super();
        this.connectionTimeout = 10000;
        this.homePath = System.getProperty("user.dir");
    }

    public boolean isCorrect(){
        return  !serverIP.isEmpty() &&
                serverPort != null &&
                !pathToSave.isEmpty() &&
                !updateType.isEmpty() &&
                connectionTimeout != null &&
                !homePath.isEmpty() &&
                !loginString.isEmpty() &&
                !passwordString.isEmpty() &&
                !execProgram.isEmpty();
    }

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public Integer getServerPort() {
        return serverPort;
    }

    public void setServerPort(Integer serverPort) {
        this.serverPort = serverPort;
    }

    public String getPathToSave() {
        return pathToSave;
    }

    public void setPathToSave(String pathToSave) {
        this.pathToSave = pathToSave;
    }

    public String getUpdateType() {
        return updateType;
    }

    public void setUpdateType(String updateType) {
        this.updateType = updateType;
    }

    public Integer getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(Integer connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public String getHomePath() {
        return homePath;
    }

    public void setHomePath(String homePath) {
        this.homePath = homePath;
    }

    public String getLoginString() {
        return loginString;
    }

    public void setLoginString(String loginString) {
        this.loginString = loginString;
    }

    public String getPasswordString() {
        return passwordString;
    }

    public void setPasswordString(String passwordString) {
        this.passwordString = passwordString;
    }

    public String getExecProgram() {
        return execProgram;
    }

    public void setExecProgram(String execProgram) {
        this.execProgram = execProgram;
    }

    @Override
    public String toString() {
        return "Settings{" +
                "serverIP='" + serverIP + '\'' +
                ", serverPort=" + serverPort +
                ", pathToSave='" + pathToSave + '\'' +
                ", updateType='" + updateType + '\'' +
                ", connectionTimeout=" + connectionTimeout +
                ", homePath='" + homePath + '\'' +
                ", loginString='" + loginString + '\'' +
                ", passwordString='" + passwordString + '\'' +
                ", execProgram='" + execProgram +
                '}';
    }
}
