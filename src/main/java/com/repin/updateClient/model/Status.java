package com.repin.updateClient.model;

import javafx.beans.property.*;

public class Status {
    private StringProperty progressText;
    private DoubleProperty progressDownload;
    private StringProperty statusLabel;

    public Status() {
        progressText = new SimpleStringProperty("Инициализация...");
        progressDownload = new SimpleDoubleProperty(0);
        statusLabel = new SimpleStringProperty("Инициализация...");
    }

    public String getProgressText() {
        return progressText.get();
    }

    public StringProperty progressTextProperty() {
        return progressText;
    }

    public void setProgressText(String progressText) {
        this.progressText.set(progressText);
    }

    public double getProgressDownload() {
        return progressDownload.get();
    }

    public DoubleProperty progressDownloadProperty() {
        return progressDownload;
    }

    public void setProgressDownload(double progressDownload) {
        this.progressDownload.set(progressDownload);
    }

    public String getStatusLabel() {
        return statusLabel.get();
    }

    public StringProperty statusLabelProperty() {
        return statusLabel;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel.set(statusLabel);
    }
}
